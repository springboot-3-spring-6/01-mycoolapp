package dev.omargt.springboot.demo.mycoolapp.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunRestController {

    // From application.properties file
    @Value("${developer.name}")
    private String developerName;
    @Value("${developer.lastname}")
    private String developerLastName;
    @Value("${developer.type}")
    private String developerType;

    // expose "/" -> "Hello World from Spring Boot App!"
    @GetMapping("/")
    public String sayHello() {
        return "Hello World from Spring Boot App!";
    }

    @GetMapping("/fun")
    public String gettingFun() {
        return "Getting fun!";
    }

    @GetMapping("/developer")
    public String getDeveloper() {
        return String.format("%s %s and his type is: %s",
                developerName,
                developerLastName,
                developerType);
    }

}
