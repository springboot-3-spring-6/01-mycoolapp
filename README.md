# A cool app with Spring (#01)
This app is for apply concepts for Spring Boot 3 and Spring 6 from
this [udemy course](https://www.udemy.com/course/spring-hibernate-tutorial/).

> From the mentioned course, this would be the first section, you can see
> other sections in this group repo in [gitlab](https://gitlab.com/springboot-3-spring-6)

## General information
In this repo, is covering the bases of Spring, like specific concepts,
how it works, etc. Each commit has an specific feature/concept added.

## Concepts learned
- Maven basics (project info, dependencies and plugins)
- Maven project structure 
- Maven parent dependency
- Spring Boot basics (Init app, application.properties file to config project)
- Spring Boot Starters (Group of Spring modules/dependencies for easy starting)
- Spring Boot Actuator
- Spring Boot Security basics
- Use properties in our app

## Requirements
- You need at least Java 17 or higher.

## Extra information
For this project, I'm using Intellij IDEA.
